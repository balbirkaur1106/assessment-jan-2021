<!DOCTYPE HTML>


<html lang="en">

<head>

</head>

<body>

    <?php

    $args = array(
        'orderby' => 'title',
        'post_type' => 'automobiles'
    );
    $the_query = new WP_Query($args);

    if ($the_query->have_posts()) : while ($the_query->have_posts()) : $the_query->the_post(); ?>
            <div class="each-automobile">
                <h1 class="the-title"><?php the_title(); ?></h1>
            </div>

        <?php endwhile;
    else : ?> <p>Sorry, there are no posts to display</p> <?php endif; ?>
    <?php wp_reset_query(); ?>
</body>

</html>