<!DOCTYPE HTML>


<html lang="en">

<head>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">

</head>

<body>
    <?php
    // define variables and set to empty values

    $nameErr1 = $nameErr2 = $nameErr3 = "";
    $healthidErr1 = $healthidErr2 = $healthidErr3 = "";

    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $error_counter = 0;
        for ($field = 1; $field <= 3; $field++) {
            $errname = "nameErr" . $field;
            $errhealthid = "healthidErr" . $field;
            $name = "name" . $field;
            $healthid = "healthid" . $field;
            if (empty($_POST[$name])) {
                $errname_text = "Name " . $field . " is required";
                $$errname = '<div class="alert alert-danger mt-2" role="alert">' . $errname_text . '</div>';
                $error_counter++;
            } else {
                $name = test_input($_POST[$name]);
                // check if name only contains letters and whitespace
                if (!preg_match("/^[a-zA-Z]{3,}/", $name)) {
                    $errname_text = "Only letters and at least 3 letters required";
                    $$errname = '<div class="alert alert-danger mt-2" role="alert">' . $errname_text . '</div>';
                    $error_counter++;
                }
            }
            if (empty($_POST[$healthid])) {
                $errhealthid_text = "Health Id " . $field . " is required";
                $$errhealthid = '<div class="alert alert-danger mt-2" role="alert">' . $errhealthid_text . '</div>';
                $error_counter++;
            } else {
                $healthid = test_input($_POST[$healthid]);
                // check if name only contains letters and whitespace
                if (!preg_match("/^[0-9]{5}$/", $healthid)) {
                    $errhealthid_text = "Only 5 digits required";
                    $$errhealthid = '<div class="alert alert-danger mt-2" role="alert">' . $errhealthid_text . '</div>';
                    $error_counter++;
                }
            }
        }
        if ($error_counter == "0") {
            echo '<div class="alert alert-success mt-2" role="alert">Form has been successfully submitted.</div>';
        }
    }

    function test_input($data)
    {
        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);
        return $data;
    }
    ?>
    <div class="container">
        <div class="row">
            <div class="col-6">
                <form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>">
                    <?php
                    for ($field = 1; $field <= 3; $field++) {
                        $errname = "nameErr" . $field;
                        $errhealthid = "healthidErr" . $field;
                        $name = "name" . $field;
                        $healthid = "healthid" . $field;

                    ?>
                        <div class="mb-3">
                            <label class="form-label">Name <?php echo $field; ?>:</label> <input class="form-control" type="text" name="name<?php echo $field; ?>" value="<?php echo $_POST[$name]; ?>">
                            <?php echo  $$errname; ?>
                        </div>
                        <div class="mb-3">
                            <label class="form-label">Health Id <?php echo $field; ?>:</label> <input class="form-control" type="text" name="healthid<?php echo $field; ?>" value="<?php echo $_POST[$healthid]; ?>">
                            <?php echo $$errhealthid; ?>
                        </div>
                    <?php
                    }
                    ?>
                    <input type="submit" name="submit" value="Submit" class="btn btn-info">
                </form>
            </div>
        </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.min.js" integrity="sha384-pQQkAEnwaBkjpqZ8RU1fF1AKtTcHJwFl3pblpTlHXybJjHpMYo79HY3hIi4NKxyj" crossorigin="anonymous"></script>

</body>

</html>